package com.example.coloredtiles

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.widget.Toast
import kotlin.random.Random


class MyView(context: Context?) : View(context) {
    private val p = Paint()
    var touchX = 0f
    var touchY = 0f
    var w = 0
    var h = 0
    var rect_size = 0
    var smaller_side = 0
    var bigger_side = 0


    val game_padding = 100
    val tiles_padding = 40
    val tiles_number = 2
    var vertical_padding = 0

    var tiles = Array(tiles_number) {BooleanArray(tiles_number) {false} }
    var flag = true
    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
    }

    override fun onDraw(canvas: Canvas?) {

        p.color = Color.RED
        canvas?.apply {
//            Log.d("Tiles", "canvas $width, $height")
            drawColor(Color.YELLOW)
        }
            drawRectOnCanvas(canvas, p)

        if (check_victory()) {
            val toast = Toast.makeText(context, "Victory!", Toast.LENGTH_LONG)
            toast.show()
        }

    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        touchX = (event?.x ?: 0) as Float
        touchY = (event?.y ?: 0) as Float

        Log.d("Tiles", "Touch X: $touchX, Y:$touchY")


        changeTiles(getRectIndexes(touchY, touchX))

        return false
    }

    fun drawRectOnCanvas(canvas: Canvas?, p :Paint) {
        canvas?.apply {
            for (i in tiles.indices) {
                for (j in tiles[i].indices) {
                    if (tiles[i][j]) {
                        p.color = Color.RED
                    } else {
                        p.color = Color.BLUE
                    }
                    drawRect(game_padding + i*rect_size.toFloat() + tiles_padding*i,
                        vertical_padding + j*rect_size.toFloat() + tiles_padding*j,
                        game_padding + (i+1)*rect_size.toFloat() + tiles_padding*i,
                        vertical_padding + (j+1)*rect_size.toFloat() + tiles_padding*j,
                        p)
                }
            }
        }
    }

    fun getRectIndexes(Y: Float, X: Float) :Pair<Int, Int> {
        for (i in 0 until tiles_number) {
            for (j in 0 until tiles_number) {
                val left = game_padding + i*rect_size.toFloat() + tiles_padding*i
                val top = vertical_padding + j*rect_size.toFloat() + tiles_padding*j
                val right = game_padding + (i+1)*rect_size.toFloat() + tiles_padding*i
                val bottom = vertical_padding + (j+1)*rect_size.toFloat() + tiles_padding*j

                if (left <= X && X <= right && top <= Y && Y <= bottom) {
                    Log.d("Tiles", "tile $i, $j")
                    return Pair(i, j)
                }
            }
        }
        return Pair(-1, -1)
    }

    fun changeTiles(tile :Pair<Int, Int>) {

        if (tile.first == -1 || tile.second == -1)
            return

        for (i in 0 until tiles_number) {
            tiles[tile.first][i] = !tiles[tile.first][i]
            tiles[i][tile.second] = !tiles[i][tile.second]
        }
        tiles[tile.first][tile.second] = !tiles[tile.first][tile.second]
        invalidate()
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)

        var genFlag = true
        while (genFlag) {
            generate_matrix()
            if (!check_victory()) {
                genFlag = false
            }
        }

        w = right - left
        h = bottom - top

        smaller_side = if (w < h) w else h
        bigger_side = if (w > h) w else h
        rect_size = (smaller_side - game_padding *2 - tiles_padding * (tiles_number - 1)) / tiles_number
        vertical_padding = (bigger_side - tiles_padding * (tiles_number - 1) - rect_size * tiles_number) / 2
        for (i in 0 until tiles_number) {
            for (j in 0 until tiles_number) {
                Log.d("Tiles", "Tile ${tiles[i][j]}")

            }
        }
        Log.d("Tiles", "Hello")
        invalidate()

    }

    private fun generate_matrix () {
        for (i in tiles.indices) {
            for (j in tiles[i].indices) {
                tiles[i][j] = Random.nextBoolean()
            }
        }
    }

    private fun check_victory() : Boolean{
        val flag1 = tiles[0][0]
        for (i in tiles.indices) {
            for (j in tiles[i].indices) {
                 if (tiles[i][j] != flag1) {
                     return false
                 }
            }
        }
        return true
    }


}